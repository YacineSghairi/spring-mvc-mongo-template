package com.gnoht.app;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;

/**
 * Tests configuration of Spring's {@link ApplicationContext} for this
 * project (e.g. classpath:spring/application-context.xml). 
 */
public class ApplicationContextIntTests 
		extends AbstractIntegrationTests {

	@Value("${app.name}")
	private String appName;
	
	/**
	 * Validates application.properties was loaded
	 */
	@Test
	public void testApplicationPropertiesLoaded() {
		assertTrue("missing app.name from application.properties", 
			appName != null && "Spring MVC with mongoDB".equals(appName));
	}
	
	
}
