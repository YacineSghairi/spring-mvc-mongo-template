package com.gnoht.app.repository.support;

import java.io.Serializable;

import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.repository.query.MongoEntityInformation;
import org.springframework.data.mongodb.repository.support.MongoRepositoryFactory;
import org.springframework.data.mongodb.repository.support.MongoRepositoryFactoryBean;
import org.springframework.data.mongodb.repository.support.QueryDslMongoRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.core.RepositoryMetadata;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;


public class MongoCustomRepositoryFactoryBean
			<R extends CrudRepository<T, ID>, T, ID extends Serializable> 
		extends MongoRepositoryFactoryBean<R, T, ID> {

	@Override
	@SuppressWarnings("rawtypes")
	protected RepositoryFactorySupport getFactoryInstance(
			MongoOperations mongoOperations) {
		return new CustomRepositoryFactory(mongoOperations);
	}

	private static class CustomRepositoryFactory<T, ID extends Serializable> 
		extends MongoRepositoryFactory {

		private MongoOperations mongoOperations;
		
		public CustomRepositoryFactory(MongoOperations mongoOperations) {
			super(mongoOperations);
			this.mongoOperations = mongoOperations;
		}
		
		@Override
		@SuppressWarnings({"unchecked", "rawtypes"})
		protected Object getTargetRepository(RepositoryMetadata metadata) {
			MongoEntityInformation<?, Serializable> entityInformation =
					getEntityInformation(metadata.getDomainType());
			
			if(getRepositoryBaseClass(metadata).getClass().equals(QueryDslMongoRepository.class)) {
				return super.getTargetRepository(metadata);
			} else {
				return new MongoCustomRepositoryImpl(entityInformation, mongoOperations);
				
			}
			
		}
	}
}
