package com.gnoht.app.repository;

import java.util.List;

import com.gnoht.app.model.Role;
import com.gnoht.app.model.User;

public interface UserRepository extends
		CustomRepository<User, String> {

	/**
	 * Finds all {@link User}s with given {@link Role}.
	 */
	public List<User> findAllByRoles(Role role);
}
