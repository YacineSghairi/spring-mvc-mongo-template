package com.gnoht.app.model;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Example {@link Document} representing a User.
 */
@Document(collection="users")
public class User {

	@Id @Field
	private String id;
	
	@Field 
	@Indexed(unique=true)
	@Size(min=2, max=50)
	private String name;
	
	@Field 
	@Indexed(unique=true)
	@Email
	@NotNull
	private String email;
	
	@DBRef
	@Field
	private Set<Role> roles = new HashSet<Role>();

	public User() {}
	
	@PersistenceConstructor
	public User(String name, String email, Set<Role> roles) {
		this.name = name;
		this.email = email;
		this.roles = roles;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getId() {
		return id;
	}

	public final Set<Role> getRoles() {
		return Collections.unmodifiableSet(roles);
	}
	public void addRole(Role role) {
		roles.add(role);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", email=" + email
				+ ", roles=" + roles + "]";
	}
}
